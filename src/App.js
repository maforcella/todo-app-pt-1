import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    value: "",
  };

  //this keeps track what is being typed into the text input field
  handleTextInput = (event) => {
    this.setState({ value: event.target.value }); //Vince does not like this way!  These should be written as functions because it saves time during the render.  Need to look into and review this further.
  };

  //this runs the code to add a new todo to the list when enter key is pressed
  handleSubmit = (event) => {
    if (event.key === "Enter") {
      this.handleAddNewTodo();
    }
  };

  //this provides the structure for adding a new todo to the list when run in conjunction with the submit function
  handleAddNewTodo = () => {
    const newTodo = {
      userId: 1,
      id: Math.floor(Math.random() * 30), //this is one way of getting a random id number, you could also download an extension that does the same thing from npm
      title: this.state.value,
      completed: false,
    };
    //this adds the new todo to the array and clears the text input field
    const updatedTodos = [...this.state.todos, newTodo];
    this.setState({
      todos: updatedTodos,
      value: "",
    });
  };

  //this allows you to remove a single todo by clicking the x next to it
  //checks to see which todo you clicked and filters it out returning a new array with everything else remaining
  handleRemoveSingleTodo = (selectedTodo) => {
    const remainingTodos = this.state.todos.filter(
      (nonSelectedTodos) => nonSelectedTodos.id !== selectedTodo
    );
    this.setState({ todos: remainingTodos });
  };

  //this handles toggling of the checkbox for each todo item
  //switches "completed" boolean value to its opposite for whichever todo checkmark box is clicked
  handleCheckMark = (targetedTodo) => {
    const toggledTodos = this.state.todos.map((todoCheckMark) => {
      if (todoCheckMark.id === targetedTodo) {
        todoCheckMark.completed = !todoCheckMark.completed;
      }
      return todoCheckMark;
    });
    this.setState({ todos: toggledTodos });
  };

  //this removes all todos with check marks when hitting the clear completed button
  //checks for and returns only todos with a "completed" value of false
  handleClearComplete = () => {
    const unfinishedTodos = this.state.todos.filter(
      (allTodos) => allTodos.completed !== true
    );
    this.setState({ todos: unfinishedTodos });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            type="text"
            onChange={this.handleTextInput}
            onKeyDown={this.handleSubmit}
            value={this.state.value}
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
          />
        </header>
        <TodoList
          todos={this.state.todos}
          handleRemoveSingleTodo={this.handleRemoveSingleTodo}
          handleCheckMark={this.handleCheckMark}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button
            onClick={this.handleClearComplete}
            className="clear-completed"
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            onChange={() => this.props.handleCheckMark(this.props.id)}
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
          />
          <label>{this.props.title}</label>
          <button
            className="destroy"
            onClick={() => this.props.handleRemoveSingleTodo(this.props.id)}
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              handleCheckMark={this.props.handleCheckMark}
              handleRemoveSingleTodo={this.props.handleRemoveSingleTodo}
              title={todo.title}
              id={todo.id}
              completed={todo.completed}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
